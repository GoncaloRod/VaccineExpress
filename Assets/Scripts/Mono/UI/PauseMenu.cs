using System;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    private InputActions _actions;
    
    private GameManager _gameManager;

    private bool _showMenu;
    
    [Space]
    [SerializeField] private GameObject pauseMenuPanel;

    private void Awake()
    {
        Debug.Assert(pauseMenuPanel != null, $"Missing pause menu panel in {name}");

        _actions = new InputActions();

        _actions.UI.Pause.performed += _ => TogglePause();
        
        pauseMenuPanel.SetActive(_showMenu);
    }

    private void Start()
    {
        _gameManager = GameManager.Instance;
    }

    public void TogglePause()
    {
        _showMenu = !_showMenu;

        if (_showMenu)
        {
            Time.timeScale = 0f;
            GameManager.Instance.OnGamePause.Invoke();
        }
        else
        {
            Time.timeScale = 1f;
            GameManager.Instance.OnGameResume.Invoke();
        }
        
        pauseMenuPanel.SetActive(_showMenu);
    }

    public void OnQuitButtonPressed()
    {
        // TODO: Return to main menu
    }

    private void OnEnable()
    {
        _actions.Enable();
    }

    private void OnDisable()
    {
        _actions.Disable();
    }
}
