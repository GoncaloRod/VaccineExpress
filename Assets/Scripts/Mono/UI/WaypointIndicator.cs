using System;
using UnityEngine;

public class WaypointIndicator : MonoBehaviour
{
    private DemandManager _demandManager;

    private Camera _camera;

    [Space]
    [SerializeField] private GameObject indicator;

    private void Awake()
    {
        _camera = Camera.main;
    }

    private void Start()
    {
        _demandManager = DemandManager.Instance;
    }

    private void Update()
    {
        Vector3 dirToWaypoint = _demandManager.CurrentRequest.Waypoint.Position.position - _camera.transform.position;

        float cameraAngle = _camera.transform.eulerAngles.y;
        float waypointAngle = Mathf.Atan2(dirToWaypoint.y, dirToWaypoint.x);
        
        indicator.transform.rotation = Quaternion.Euler(0f, 0f, waypointAngle + cameraAngle + 160f);
    }
}
