using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UpgradesMenu : MonoBehaviour
{
    private InputActions _actions;

    private UpgradeManager _upgradeManager;

    private bool _showMenu = false;

    [Space]
    [SerializeField] private GameObject upgradeMenuPanel;

    [Header("Capacity Update")]
    [SerializeField] private Button capacityUpgradeButton;
    [SerializeField] private TMP_Text capacityCostText;
    
    [Header("Refill Quantity Update")]
    [SerializeField] private Button refillQuantityUpgradeButton;
    [SerializeField] private TMP_Text refillQuantityCostText;
    
    [Header("Refill Speed Update")]
    [SerializeField] private Button refillSpeedUpgradeButton;
    [SerializeField] private TMP_Text refillSpeedCostText;

    private void Awake()
    {
        _actions = new InputActions();

        _actions.UI.Upgrades.performed += _ => ToggleMenu();
        
        upgradeMenuPanel.SetActive(_showMenu);
        
        capacityUpgradeButton.onClick.AddListener(OnCapacityUpgradeButtonPressed);
        refillQuantityUpgradeButton.onClick.AddListener(OnRefillQuantityUpgradeButtonPressed);
        refillSpeedUpgradeButton.onClick.AddListener(OnRefillSpeedUpgradeButtonPressed);
    }

    private void Start()
    {
        _upgradeManager = UpgradeManager.Instance;
    }

    void ToggleMenu()
    {
        _showMenu = !_showMenu;

        if (_showMenu)
        {
            Time.timeScale = 0f;
            GameManager.Instance.OnGamePause.Invoke();
            UpdateMenu();
        }
        else
        {
            Time.timeScale = 1f;
            GameManager.Instance.OnGameResume.Invoke();
        }
        
        upgradeMenuPanel.SetActive(_showMenu);
    }

    private void UpdateMenu()
    {
        capacityCostText.text = $"Cost: {_upgradeManager.VaccineCapacityUpgradeCost} Points";
        capacityUpgradeButton.interactable = _upgradeManager.CanUpgradeVaccineCapacity;

        refillQuantityCostText.text = $"Cost: {_upgradeManager.VaccinesPerRefillUpgradeCost} Points";
        refillQuantityUpgradeButton.interactable = _upgradeManager.CanUpgradeVaccinesPerRefill;

        refillSpeedCostText.text = $"Cost: {_upgradeManager.RefillIntervalUpgradeCost} Points";
        refillSpeedUpgradeButton.interactable = _upgradeManager.CanUpgradeRefillInterval;
    }

    private void OnCapacityUpgradeButtonPressed()
    {
        _upgradeManager.UpgradeCapacity();
        UpdateMenu();
    }
    
    private void OnRefillQuantityUpgradeButtonPressed()
    {
        _upgradeManager.UpgradeRefillQuantity();
        UpdateMenu();
    }
    
    private void OnRefillSpeedUpgradeButtonPressed()
    {
        _upgradeManager.UpgradeRefillSpeed();
        UpdateMenu();
    }

    private void OnEnable()
    {
        _actions.Enable();
    }

    private void OnDisable()
    {
        _actions.Disable();
    }
}
