using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StatsUI : MonoBehaviour
{
    private VehicleManager _vehicleManager;
    private DemandManager _demandManager;
    private PlayerStats _playerStats;

    [Header("Player Stats")]
    [SerializeField] private TMP_Text pointsCounterText;
    
    [Header("Vehicle Stats")]
    [SerializeField] private TMP_Text vaccineCounterText;

    [Header("Request Stats")]
    [SerializeField] private Slider timeLeftSlider;
    [SerializeField] private Image timeLeftSliderBackground;
    [SerializeField] private Gradient timeLeftSliderBackgroundColor;
    [SerializeField] private TMP_Text timeLeftValue;
    [Space]
    [SerializeField] private TMP_Text locationValue;
    [SerializeField] private TMP_Text amountValue;
    
    private void Start()
    {
        _vehicleManager = VehicleManager.Instance;
        _demandManager = DemandManager.Instance;
        _playerStats = PlayerStats.Instance;
    }

    private void Update()
    {
        UpdatePlayerStats();
        
        UpdateVehicleStats();
        
        UpdateRequestStats();
    }

    private void UpdatePlayerStats()
    {
        pointsCounterText.text = $"Points: {_playerStats.Points}";
    }

    private void UpdateVehicleStats()
    {
        var vehicleStats = _vehicleManager.ActiveVehicle.Stats;
        
        if (vehicleStats == null)
        {
            vaccineCounterText.gameObject.SetActive(false);
        }
        else
        {
            vaccineCounterText.gameObject.SetActive(true);
            vaccineCounterText.text = $"{vehicleStats.TransportedVaccines} / {vehicleStats.VaccineCapacity}";
        }
    }

    private void UpdateRequestStats()
    {
        var currentRequest = _demandManager.CurrentRequest;

        float timeLeftNormalized = _demandManager.TimeLeft / currentRequest.TimeToDeliver;

        timeLeftSlider.value = timeLeftNormalized;
        timeLeftSliderBackground.color = timeLeftSliderBackgroundColor.Evaluate(timeLeftNormalized);
        timeLeftValue.text = $"{_demandManager.TimeLeft:F0} sec.";
        
        locationValue.text = currentRequest.Waypoint.WaypointName;
        amountValue.text = $"{currentRequest.RequestedAmount} Vaccines";
    }
}
