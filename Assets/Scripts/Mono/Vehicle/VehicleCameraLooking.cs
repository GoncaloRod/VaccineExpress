using System;
using Cinemachine;
using UnityEngine;

[RequireComponent(typeof(CinemachineFreeLook))]
public class VehicleCameraLooking : MonoBehaviour
{
    private InputActions _input;

    private CinemachineFreeLook _camera;

    [Space]
    [SerializeField] private float sensitivity = 1f;

    private void Awake()
    {
        _input = new InputActions();

        _camera = GetComponent<CinemachineFreeLook>();
    }

    private void Update()
    {
        Vector2 lookingInput = _input.Vehicle.Looking.ReadValue<Vector2>();

        _camera.m_YAxis.Value += -lookingInput.y * (sensitivity / 100 * Time.deltaTime);
        _camera.m_XAxis.Value += lookingInput.x * (sensitivity * Time.deltaTime);
    }

    private void OnEnable()
    {
        _input.Enable();
    }

    private void OnDisable()
    {
        _input.Disable();
    }
}
