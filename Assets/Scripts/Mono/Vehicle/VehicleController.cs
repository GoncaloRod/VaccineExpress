using System;
using System.Linq;
using UnityEngine;

[Serializable]
struct Wheel
{
    public WheelCollider collider;
    public GameObject graphicsObject;

    public bool traction;
    public bool steer;
}

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(VehicleInput))]
public class VehicleController : MonoBehaviour
{
    private const float MpsToKph = 3.6f;
    
    private Rigidbody _rigidbody;
    private VehicleInput _input;

    private float _velocity;

    private int _currentGear = 1;
    
    private Wheel[] _steerWheels;
    private Wheel[] _tractionWheels;

    [Header("Vehicle Setup")]
    [SerializeField] private Transform COG;
    [Space]
    [SerializeField] private Transform vehicleExit;
    [Space]
    [SerializeField] private Wheel[] wheels;

    [Header("Vehicle Settings")]
    [SerializeField] private float forwardTorque = 100f;
    [SerializeField] private float reverseTorque = 100f;
    [SerializeField] private float brakeTorque = 100f;
    [Space]
    [SerializeField] private float maxSteer = 30f;

    private void Awake()
    {
        Debug.Assert(COG != null, $"Vehicle's center of gravity not set in {name}");
        
        _rigidbody = GetComponent<Rigidbody>();
        _input = GetComponent<VehicleInput>();

        _rigidbody.centerOfMass = COG.localPosition;

        _steerWheels = wheels.Where(w => w.steer).ToArray();
        _tractionWheels = wheels.Where(w => w.traction).ToArray();
    }

    private void Update()
    {
        _velocity = _rigidbody.velocity.magnitude * MpsToKph;

        if (_velocity * _currentGear <= 0.1f && _input.BrakeInput > 0f)
        {
            _currentGear = -1;
        }
        else
        {
            _currentGear = 1;
        }

        float torqueOutput = 0f;
        float brakeOutput = 0f;
        
        if (_currentGear > 0)
        {
            torqueOutput = forwardTorque * _input.AcceleratorInput;
            brakeOutput = brakeTorque * _input.BrakeInput;
        }
        else if (_currentGear < 0)
        {
            torqueOutput = -reverseTorque * _input.BrakeInput;
            brakeOutput = brakeTorque * _input.AcceleratorInput;
        }
        
        foreach (var wheel in _tractionWheels)
        {
            wheel.collider.motorTorque = torqueOutput / _tractionWheels.Length;
            wheel.collider.brakeTorque = brakeOutput;
        }
        
        float steerAngle = maxSteer * _input.SteerInput;

        foreach (var wheel in _steerWheels)
        {
            wheel.collider.steerAngle = steerAngle;
        }

        foreach (Wheel wheel in wheels)
        {
            Vector3 pos;
            Quaternion rot;
            
            wheel.collider.GetWorldPose(out pos, out rot);

            wheel.graphicsObject.transform.position = pos;
            wheel.graphicsObject.transform.rotation = rot;
        }
        
        if (_input.ExitVehicle)
            ExitVehicle();
    }

    private void ExitVehicle()
    {
        var player = VehicleManager.Instance.Player;
        
        player.transform.position = vehicleExit.position;

        _input.enabled = false;
        
        VehicleManager.Instance.OnGetOutVehicle();

        _input.ExitVehicle = false;
    }
}
