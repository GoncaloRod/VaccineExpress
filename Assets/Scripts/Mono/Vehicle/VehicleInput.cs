using System;
using UnityEngine;
using UnityEngine.Events;

public class VehicleInput : MonoBehaviour
{
    private InputActions _actions;

    private bool _previousActiveState;
    
    public float AcceleratorInput { get; private set; }
    
    public float BrakeInput { get; private set; }
    
    public float SteerInput { get; private set; }

    public bool ExitVehicle { get; set; }

    private void Awake()
    {
        _actions = new InputActions();

        _actions.Walking.Interact.performed += _ => ExitVehicle = true;
        _actions.Walking.Interact.canceled += _ => ExitVehicle = false;
    }

    private void Start()
    {
        GameManager.Instance.OnGamePause.AddListener(() =>
        {
            _previousActiveState = enabled;
            enabled = false;
        });
        
        GameManager.Instance.OnGameResume.AddListener(() =>
        {
            enabled = _previousActiveState;
        });
    }

    private void Update()
    {
        AcceleratorInput = _actions.Vehicle.Accelerator.ReadValue<float>();
        BrakeInput = _actions.Vehicle.Brake.ReadValue<float>();
        SteerInput = _actions.Vehicle.Steer.ReadValue<float>();
    }

    private void OnEnable()
    {
        _actions.Enable();
    }

    private void OnDisable()
    {
        _actions.Disable();

        AcceleratorInput = BrakeInput = SteerInput = 0f;
    }
}
