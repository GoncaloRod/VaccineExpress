using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleStats : MonoBehaviour
{
    private UpgradeManager _upgradeManager;
    
    private int _vaccineCapacity;

    public int VaccineCapacity => _vaccineCapacity;
    
    public int TransportedVaccines { get; private set; } = 0;

    private void Start()
    {
        _upgradeManager = UpgradeManager.Instance;
    }

    private void Update()
    {
        _vaccineCapacity = _upgradeManager.VaccineCapacity;
    }

    public void AddVaccines(int amount)
    {
        TransportedVaccines += amount;

        if (TransportedVaccines > _vaccineCapacity)
            TransportedVaccines = _vaccineCapacity;
    }

    public int RequestVaccines(int amount)
    {
        if (TransportedVaccines >= amount)
        {
            TransportedVaccines -= amount;
            return amount;
        }
        else
        {
            int vaccinesToReturn = TransportedVaccines;
            TransportedVaccines = 0;
            return vaccinesToReturn;
        }
    }
}
