using System;
using UnityEngine;

[RequireComponent(typeof(VehicleInput))]
public class GetInVehicle : MonoBehaviour, IInteractable
{
    private VehicleManager _vehicleManager;
    
    private VehicleInput _input;

    private void Awake()
    {
        _vehicleManager = VehicleManager.Instance;
        
        _input = GetComponent<VehicleInput>();
    }

    public void OnInteract()
    {
        _vehicleManager.OnGetInVehicle(gameObject);
        _input.enabled = true;
    }
}
