using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class WaypointTrigger : MonoBehaviour
{
    public Action<VehicleStats> OnEnter;

    private void OnTriggerEnter(Collider other)
    {
        var stats = other.GetComponent<VehicleStats>();

        if (stats != null)
            OnEnter(stats);
    }
}
