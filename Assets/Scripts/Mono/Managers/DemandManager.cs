using System;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

[Serializable]
public class Waypoint
{
    public string WaypointName;
    public Transform Position;
}

public class Request
{
    public Waypoint Waypoint;
    public float TimeToDeliver;
    public int RequestedAmount;
}

public class DemandManager : MonoBehaviour
{
    public static DemandManager Instance;

    private UpgradeManager _upgradeManager;
    private PlayerStats _playerStats;
    
    private GameObject _waypointTrigger;

    private int _currentWaypointIndex = -1;

    [Space]
    [SerializeField] private Waypoint[] waypoints;

    [Space]
    [SerializeField] private GameObject waypointPrefab;
    
    public Request CurrentRequest { get; private set; }

    public float TimeLeft { get; private set; }
    
    public UnityEvent OnNewWaypoint;
    
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }

        OnNewWaypoint = new UnityEvent();
        
        Debug.Assert(waypoints.Length > 0, $"Missing waypoints in {name}");
        
        _waypointTrigger = Instantiate(waypointPrefab, transform.position, Quaternion.identity);
        _waypointTrigger.GetComponent<WaypointTrigger>().OnEnter += OnVehicleEnterWaypoint;
        _waypointTrigger.SetActive(false);
    }

    private void Start()
    {
        _upgradeManager = UpgradeManager.Instance;
        _playerStats = PlayerStats.Instance;
        
        GenerateNewRequest();
    }

    private void Update()
    {
        if (CurrentRequest == null)
        {
            GenerateNewRequest();
        }
        else
        {
            TimeLeft -= Time.deltaTime;

            if (TimeLeft <= 0f)
            {
                TimeLeft = 0f;
                _playerStats.RequestPoints(CurrentRequest.RequestedAmount / 2);
                GenerateNewRequest();
            }
        }
    }

    private void GenerateNewRequest()
    {
        int nextWaypoint;

        do
        {
            nextWaypoint = Random.Range(0, waypoints.Length);
        } while (nextWaypoint == _currentWaypointIndex);

        _currentWaypointIndex = nextWaypoint;
        
        CurrentRequest = new Request()
        {
            Waypoint = waypoints[_currentWaypointIndex],
            RequestedAmount = Random.Range(_upgradeManager.VaccineCapacity / 4, _upgradeManager.VaccineCapacity / 2),
            TimeToDeliver = Random.Range(1 * 60, 3 * 60)
        };

        TimeLeft = CurrentRequest.TimeToDeliver;

        _waypointTrigger.transform.position = CurrentRequest.Waypoint.Position.position;
        _waypointTrigger.SetActive(true);

        OnNewWaypoint.Invoke();
    }

    private void OnVehicleEnterWaypoint(VehicleStats stats)
    {
        int deliveredVaccines = stats.RequestVaccines(CurrentRequest.RequestedAmount);

        CurrentRequest.RequestedAmount -= deliveredVaccines;
        _playerStats.AddPoints(deliveredVaccines);

        if (CurrentRequest.RequestedAmount == 0)
        {
            GenerateNewRequest();
        }
    }
}
