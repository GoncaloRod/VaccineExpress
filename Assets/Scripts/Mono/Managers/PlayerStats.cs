using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public static PlayerStats Instance;

    public int Points { get; private set; } = 0;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    public void AddPoints(int amount)
    {
        Points += amount;
    }

    public int RequestPoints(int amount)
    {
        Points -= amount;
        return amount;
    }
}
