using System;
using UnityEngine;

public class UpgradeManager : MonoBehaviour
{
    public static UpgradeManager Instance;

    private PlayerStats _playerStats;

    public int VaccineCapacity { get; private set; } = 100;
    public int VaccineCapacityUpgradeCost { get; private set; } = 150;
    public bool CanUpgradeVaccineCapacity => _playerStats.Points >= VaccineCapacityUpgradeCost;

    public int VaccinesPerRefill { get; private set; } = 10;
    public int VaccinesPerRefillUpgradeCost { get; private set; } = 200;
    public bool CanUpgradeVaccinesPerRefill => _playerStats.Points >= VaccinesPerRefillUpgradeCost;

    public float RefillInterval { get; private set; } = 1f;
    public int RefillIntervalUpgradeCost { get; private set; } = 250;
    public bool CanUpgradeRefillInterval => _playerStats.Points >= RefillIntervalUpgradeCost;    
    
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        _playerStats = PlayerStats.Instance;
    }

    public void UpgradeCapacity()
    {
        _playerStats.RequestPoints(VaccineCapacityUpgradeCost);
        
        VaccineCapacity *= 2;
        VaccineCapacityUpgradeCost += VaccineCapacityUpgradeCost / 4;
    }

    public void UpgradeRefillQuantity()
    {
        _playerStats.RequestPoints(VaccineCapacityUpgradeCost);
        
        VaccinesPerRefill *= 2;
        VaccinesPerRefillUpgradeCost += VaccineCapacityUpgradeCost / 4;
    }

    public void UpgradeRefillSpeed()
    {
        _playerStats.RequestPoints(RefillIntervalUpgradeCost);
        
        RefillInterval -= RefillInterval / 4f;
        RefillIntervalUpgradeCost *= 2;
    }
}
