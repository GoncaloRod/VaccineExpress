using Cinemachine;
using UnityEngine;

public struct VehicleData
{
    public VehicleInput Input;
    public VehicleController Controller;
    public VehicleStats Stats;
}

public class VehicleManager : MonoBehaviour
{
    public static VehicleManager Instance;

    private CinemachineFreeLook _vehicleCamera;

    private VehicleData _activeVehicle;
    
    public GameObject Player { get; private set; }

    public VehicleData ActiveVehicle => _activeVehicle;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
        
        Player = GameObject.FindWithTag("Player");

        _vehicleCamera = GameObject.FindWithTag("VehicleCamera").GetComponent<CinemachineFreeLook>();
    }

    public void OnGetInVehicle(GameObject vehicle)
    {
        Player.SetActive(false);

        _activeVehicle.Input = vehicle.GetComponent<VehicleInput>();
        _activeVehicle.Controller = vehicle.GetComponent<VehicleController>();
        _activeVehicle.Stats = vehicle.GetComponent<VehicleStats>();

        _vehicleCamera.Follow = vehicle.transform;
        _vehicleCamera.LookAt = vehicle.transform;
    }
    
    public void OnGetOutVehicle()
    {
        Player.SetActive(true);

        _activeVehicle.Input = null;
        _activeVehicle.Controller = null;
        _activeVehicle.Stats = null;
    }
}
