using System;
using UnityEngine;
using UnityEngine.Events;

public class FirstPersonInput : MonoBehaviour
{
    private InputActions _inputActions;

    private bool _previousActiveState;
    
    public Vector2 MovementInput { get; private set; }
    
    public Vector2 LookInput { get; private set; }
    
    public bool Jump { get; set; }
    
    public bool Run { get; private set; }

    public UnityEvent OnInteract;

    private void Awake()
    {
        OnInteract = new UnityEvent();
        
        _inputActions = new InputActions();

        _inputActions.Walking.Jump.performed += _ => Jump = true;
        _inputActions.Walking.Jump.canceled += _ => Jump = false;

        _inputActions.Walking.Run.performed += _ => Run = true;
        _inputActions.Walking.Run.canceled += _ => Run = false;

        _inputActions.Walking.Interact.performed += _ => OnInteract.Invoke();
    }

    private void Start()
    {
        GameManager.Instance.OnGamePause.AddListener(() =>
        {
            _previousActiveState = enabled;
            enabled = false;
        });
        
        GameManager.Instance.OnGameResume.AddListener(() =>
        {
            enabled = _previousActiveState;
        });
    }

    private void Update()
    {
        MovementInput = _inputActions.Walking.Movement.ReadValue<Vector2>();
        LookInput = _inputActions.Walking.Looking.ReadValue<Vector2>();
    }

    private void OnEnable()
    {
        _inputActions.Enable();
    }

    private void OnDisable()
    {
        _inputActions.Disable();

        MovementInput = LookInput = Vector2.zero;
        Jump = Run = false;
    }
}
