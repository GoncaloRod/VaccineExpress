using System;
using Cinemachine;
using UnityEngine;

[RequireComponent(typeof(FirstPersonInput))]
[RequireComponent(typeof(CharacterController))]
public class FirstPersonController : MonoBehaviour
{
    private FirstPersonInput _input;
    private CharacterController _controller;

    private bool _previousIsGrounded;
    private bool _isGrounded;

    private float _verticalRotation = 0f;
    private float _horizontalRotation = 0f;

    private Vector3 _verticalVelocity;

    [Space] [SerializeField] private CinemachineVirtualCamera _camera;

    [Header("Controller Settings")] [SerializeField]
    private float mouseSensitivity = 1f;

    [SerializeField] [Range(0, 90)] private float maxVerticalRotation = 45f;
    [Space] [SerializeField] private float walkingSpeed = 1f;
    [SerializeField] private float runningSpeed = 2f;
    [Space] [SerializeField] private float jumpHeight = 1f;

    [Header("Ground Check")]
    [SerializeField] private Transform groundCheck;
    [SerializeField] private float groundCheckRadius = 0.4f;
    [SerializeField] private LayerMask groundCheckLayer;
    
    [Header("Interactions")]
    [SerializeField] private float interactionDistance = 2f;

    private void Awake()
    {
        _isGrounded = _previousIsGrounded = true;

        _input = GetComponent<FirstPersonInput>();
        _controller = GetComponent<CharacterController>();
    }

    private void Start()
    {
        _input.OnInteract.AddListener(Interact);
    }

    private void Update()
    {
        _previousIsGrounded = _isGrounded;
        Collider[] others = Physics.OverlapSphere(groundCheck.position, groundCheckRadius, groundCheckLayer.value,
            QueryTriggerInteraction.Ignore);
        _isGrounded = others.Length != 0;

        if (!_previousIsGrounded && _isGrounded)
            OnLand();

        HandleLook();

        HandleMovement();
    }

    private void HandleLook()
    {
        Vector2 lookInput = _input.LookInput;

        _verticalRotation += -lookInput.y * mouseSensitivity * Time.deltaTime;
        _verticalRotation = Mathf.Clamp(_verticalRotation, -maxVerticalRotation, maxVerticalRotation);

        _horizontalRotation += lookInput.x * mouseSensitivity * Time.deltaTime;

        _camera.transform.localRotation = Quaternion.Euler(_verticalRotation, 0f, 0f);

        transform.localRotation = Quaternion.Euler(0f, _horizontalRotation, 0f);
    }

    private void HandleMovement()
    {
        Vector2 movementInput = _input.MovementInput;

        Vector3 dir = transform.forward * movementInput.y + transform.right * movementInput.x;

        float speed = _input.Run ? runningSpeed : walkingSpeed;

        _controller.Move(dir * (speed * Time.deltaTime));

        if (!_isGrounded)
            _verticalVelocity += Physics.gravity * Time.deltaTime;

        if (_input.Jump && _isGrounded)
        {
            _verticalVelocity += transform.up * Mathf.Sqrt(jumpHeight * -2f * Physics.gravity.y);
            _input.Jump = false;
        }

        _controller.Move(_verticalVelocity * Time.deltaTime);
    }

    private void Interact()
    {
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, interactionDistance))
        {
            IInteractable interactable = hit.rigidbody.gameObject.GetComponent<IInteractable>();
            
            if (interactable != null)
                interactable.OnInteract();
        }
    }

    private void OnLand()
    {
        _verticalVelocity = Vector3.zero;
    }

    private void OnDrawGizmos()
    {
        if (groundCheck == null)
            return;
        
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(groundCheck.position, groundCheckRadius);
    }
}
