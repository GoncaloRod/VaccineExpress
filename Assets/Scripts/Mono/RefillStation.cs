using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class RefillStation : MonoBehaviour
{
    private UpgradeManager _upgradeManager;
    
    private VehicleStats _currentlyFilling;

    private void Start()
    {
        _upgradeManager = UpgradeManager.Instance;
    }

    private IEnumerator Refill()
    {
        while (_currentlyFilling != null)
        {
            if (_currentlyFilling.TransportedVaccines < _currentlyFilling.VaccineCapacity)
            {
                _currentlyFilling.AddVaccines(_upgradeManager.VaccinesPerRefill);
            }
            
            yield return new WaitForSeconds(_upgradeManager.RefillInterval);
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        VehicleStats stats = other.gameObject.GetComponent<VehicleStats>();

        if (stats != null && _currentlyFilling == null)
        {
            _currentlyFilling = stats;
            StartCoroutine(Refill());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        VehicleStats stats = other.gameObject.GetComponent<VehicleStats>();

        if (stats == _currentlyFilling)
        {
            StopCoroutine(Refill());
            _currentlyFilling = null;
        }
    }
}
